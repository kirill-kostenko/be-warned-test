@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit category') }} {{ $category->title }}</div>

                <div class="card-body">
                {{ HTML::ul($errors->all()) }}

                {{ Form::model($category, array('route' => array('categories.update', $category->id), 'method' => 'PUT')) }}

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', null, array('class' => 'form-control')) }}
                    </div>
                    {{ Form::submit('Edit', array('class' => 'btn btn-success')) }}

                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
