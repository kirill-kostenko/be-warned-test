@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create new category') }}</div>

                <div class="card-body">
                    {{ HTML::ul($errors->all()) }}
                    
                    {{ Form::open(array('url' => 'categories')) }}

                    <div class="form-group">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', Request::old('name'), array('class' => 'form-control')) }}
                    </div>

                    {{ Form::submit('Create', array('class' => 'btn btn-success')) }}

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
