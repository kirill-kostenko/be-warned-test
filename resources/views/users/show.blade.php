@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">User: {{ $user->name }}</div>

                <div class="card-body">
                    <p>
                        <label class="font-weight-bold">Name:</label>
                        {{ $user->name }}
                    </p>
                    <p>
                        <strong>Email:</strong>
                        {{ $user->email }}
                    </p>
                    @if($user->created_at)
                        <p>
                            <strong>Created at:</strong>
                            {{ $user->created_at }}
                        </p>
                    @endif
                    <p>
                        <strong>User has {{$user->posts->count()}} post</strong><br>

                        @foreach($user->posts as $post)
                            <a href="{{ route('posts.show', $post->id) }}">{{ $post->title }}</a><br>
                        @endforeach
                    </p>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
