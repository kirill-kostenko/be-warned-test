@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Post: {{ $post->title }}</div>

                <div class="card-body">
                    <p>
                        <label class="font-weight-bold">Title:</label>
                        {{ $post->title }}
                    </p>

                    @if($post->category)
                        <p>
                            <strong>Category:</strong>
                            {{ $post->category->name }}
                        </p>
                    @endif 

                    <p>
                        <strong>Body:</strong>
                        {{ $post->body }}
                    </p>

                    <p>
                        <strong>Author Name:</strong>
                        {{ $post->author->name }}<br>
                        <strong>Author Email:</strong>
                        {{ $post->author->email }}<br>
                    </p>

                    <p>
                        <strong>{{ $post->inFavoriteList->count() }} users add to favorite list</strong>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
