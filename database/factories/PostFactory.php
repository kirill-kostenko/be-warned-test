<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'attach_image' => $faker->imageUrl(400, 400, 'cats', false),
        'author_id' => App\User::all()->random()->id,
        'category_id' => App\Category::all()->random()->id,
        'published_at' => $faker->dateTimeThisYear($max = 'now'),
    ];
});
