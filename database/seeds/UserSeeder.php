<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 5)->create();

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => '21_garnett@list.ru',
            'password' => Hash::make('password'),
        ]);
    }
}
