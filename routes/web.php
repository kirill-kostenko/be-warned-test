<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

Route::get('/posts', 'HomeController@posts')->name('posts.index');
Route::get('/posts/{post}', 'HomeController@post')->name('posts.show');
Route::get('/users', 'HomeController@users')->name('users.index');
Route::get('/users/{user}', 'HomeController@user')->name('users.show');

Route::resource('categories', 'CategoryController', ['except' => ['show']]);

