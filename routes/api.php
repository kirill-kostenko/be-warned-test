<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    ['namespace' => 'API'], 
    function () {
        Route::group(
            ['prefix' => 'auth'], 
            function () {
                Route::post('login', 'AuthController@login');
                Route::post('signup', 'AuthController@signup');
            }
        );

        Route::group([
            'middleware' => 'auth:api'
          ], function() {

            Route::group(
                ['prefix' => 'posts'], 
                function () {
                    Route::get('/', 'PostController@index');
                    Route::get('/{post}', 'PostController@show');
                    Route::get('/user/{user}', 'PostController@userPosts');
                    Route::get('/category/{category}', 'PostController@categoryPosts');
                    Route::post('/create', 'PostController@create');
                    Route::delete('/{post}/destroy', 'PostController@delete');
                }
            );

            Route::group(
                ['prefix' => 'favorite-posts'], 
                function () {
                    Route::get('/', 'PostController@favoritePosts');
                    Route::get('/{post}', 'PostController@favoriteAddPost');
                    Route::delete('/{post}/destroy', 'PostController@favoriteDeletePost');
                }
            );

            Route::group(
                ['prefix' => 'auth'], 
                function () {
                    Route::get('logout', 'AuthController@logout');
                }
            );
        });
    }
);