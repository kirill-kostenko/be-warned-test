<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Storage;

use App\Post;
use App\User;
use App\Category;

use App\Http\Resources\Post as PostResource;
use Auth;
use Carbon\Carbon;

class PostController extends Controller
{
    public function index()
    {
        return PostResource::collection(Post::paginate());
    }

    public function show($post)
    {
        return PostResource::collection(Post::where('id', $post)->get());
    }

    public function userPosts(User $user)
    {
        return PostResource::collection(Post::ofAuthor($user->id)->get());
    }

    public function categoryPosts(Category $category)
    {
        return PostResource::collection(Post::ofCategory($category->id)->get());
    }

    public function create(PostRequest $request)
    {
        try {
            $request->request->set('author_id', Auth::id());
            $request->request->set('published_at', Carbon::now());
            $post = Post::create($request->request->all());

            if ($request->hasFile('image')) {
                $path = $request->file('image')->store('posts');

                $post->attach_image = $path;
                $post->save();
            }

            return response()->json([
                'data' => 'Post was created'
            ]);
        } catch(\Exception $e) {
           return response()->json([
                'data' => $e->getMessage()
           ], 404);
        }
    }

    public function delete($post)
    {
        $post = Post::find($post);
        
        if($post->author_id == Auth::id()){
            $post->delete();
        } else {
            return response()->json([
                'data' => 'Post not found or this post is not yours'
            ], 404);
        }

        return response()->json([
            'data' => 'Post was deleted'
        ], 404);
    }

    public function favoritePosts()
    {
        $user = Auth::user();
        $favoritePosts = $user->favoritePosts;
        if($user->favoritePosts->count() > 0) {
            return PostResource::collection($favoritePosts);
        } else {
            return response()->json([
                'data' => 'User doesnt have any favorite posts'
            ]);
        }

    }

    public function favoriteAddPost(Post $post)
    {
        try {
            $user = Auth::user();
            $user->favoritePosts()->attach($post->id);
        } catch(\Exception $e) {
            return response()->json([
                 'data' => $e->getMessage()
            ], 404);
        }
        return response()->json([
            'data' => 'Post was attached to users favorite posts'
        ]);
    }

    public function favoriteDeletePost(Post $post)
    {
        try {
            $user = Auth::user();
            $user->favoritePosts()->detach($post->id);

        } catch(\Exception $e) {
            return response()->json([
                 'data' => $e->getMessage()
            ], 404);
        }

        return response()->json([
            'data' => 'Post was detach from users favorite posts'
        ]);
    }
}
