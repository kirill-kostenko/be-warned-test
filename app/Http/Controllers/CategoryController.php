<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Category;
use Session;


class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'       => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput($request->all());
        } else {
            $category = Category::create($request->all());

            Session::flash('message', 'Successfully created category!');
            return redirect('categories');
        }
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $rules = array(
            'name'       => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput($request->all());
        } else {
            $category->name = $request->name;
            $category->save();

            Session::flash('message', 'Successfully update category!');
            return redirect('categories');
        }
    }

    public function destroy(Category $category)
    {
        $category->delete();

        Session::flash('message', 'Successfully deleted the category!');
        return redirect('categories');
    }
}
