<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function posts()
    {
        $posts = Post::paginate();
        return view('posts.index', compact('posts'));
    }

    public function post(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function users()
    {
        $users = User::paginate();
        return view('users.index', compact('users'));
    }

    public function user(User $user)
    {
        return view('users.show', compact('user'));
    }
}
