<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Author;
use App\Http\Resources\Category as CategoryResource;
use App\User;
use App\Category;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'attach_image' => $this->when($this->attach_image, $this->attach_image),
            'author' => Author::collection(User::where('id', $this->author_id)->get()),
            'category' => $this->when($this->category_id, CategoryResource::collection(Category::where('id', $this->category_id)->get())),
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
        ];
    }
}
