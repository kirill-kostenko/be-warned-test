<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'body',
        'attach_image',
        'category_id',
        'author_id', 
        'published_at'
    ];

    protected $dates = ['published_at'];

    public function scopeofAuthor($query, $user)
    {
        return $query->where('author_id', $user);
    }

    public function scopeOfCategory($query, $category)
    {
        return $query->where('category_id', $category);
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function inFavoriteList()
    {
        return $this->belongsToMany('App\User', 'favorite_posts');
    }
}
