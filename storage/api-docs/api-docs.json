{
  "openapi": "3.0.0",
  "info": {
    "title": "beWarned API",
    "description": "beWarned API documentation",
    "contact": {
      "email": "admin@be-warned.test"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": "http://be-warned.test/api",
      "description": "beWarned API backend Server"
    }
  ],
  "components": {
    "schemas": {
      "UserLogin": {
        "type": "object",
        "properties": {  
          "email": {
            "type": "string",
            "example": "admin@be-warned.test"
          },
          "password": {
            "type": "string",
            "example": "password"
          }
        }
      },
      "User": {
        "type": "object",
        "properties": {   
          "name": {
            "type": "string",
            "example": "Kirill"
          },
          "email": {
            "type": "string",
            "example": "admin@be-warned.test"
          },
          "password": {
            "type": "string",
            "example": "password"
          },
          "password_confirmation": {
            "type": "string",
            "example": "password"
          }
        }
      },
      "Post": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string",
            "example": "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
          },
          "body": {
            "type": "string",
            "example": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          "category_id": {
            "type": "integer",
            "example": 2
          }
        }
      },
      "Category": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          }
        }
      }
    },
    "securitySchemes": {
      "bearerAuth": {
        "type": "http",
        "scheme": "bearer",
        "bearerFormat": "JWT"
      }
    }
  },
  "paths": {
    "/auth/login": {
      "post": {
        "tags": [
          "Users"
        ],
        "summary": "Authenticates a user",
        "description": "Authenticates a user by provided credentials",
        "operationId": "login",
        "requestBody": {
          "description": "User's data",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserLogin"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/auth/signup": {
      "post": {
        "tags": [
          "Users"
        ],
        "summary": "Sign up a user",
        "description": "Sign up",
        "operationId": "signup",
        "requestBody": {
          "description": "User's data",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/User"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/auth/logout": {
      "get": {
        "tags": [
          "Users"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "summary": "Logout",
        "description": "Logout User",
        "operationId": "logout",
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/posts": {
      "get": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "All posts with pagination",
        "description": "All posts with pagination",
        "operationId": "All posts with pagination",
        
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/posts/user/{user_id}": {
      "get": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "All posts by user",
        "description": "All posts by user",
        "operationId": "All posts by user",
        "parameters": [
          {
            "name": "user_id",
            "required": true,
            "type": "integer",
            "in": "path"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/posts/category/{category_id}": {
      "get": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "All posts by category",
        "description": "All posts by category",
        "operationId": "All posts by category",
        "parameters": [
          {
            "name": "category_id",
            "required": true,
            "type": "integer",
            "in": "path"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/posts/{post_id}": {
      "get": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "Post by id",
        "description": "Post by id",
        "operationId": "Post by id",
        "parameters": [
          {
            "name": "post_id",
            "required": true,
            "type": "integer",
            "in": "path"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/posts/create": {
      "post": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "Create new post",
        "description": "Create new post",
        "operationId": "Create new post",

        "consumes": "multipart/form-data",
        "parameters": [{
          "in": "formData",
          "name": "image",
          "type": "file"
        }],

        "requestBody": {
          "description": "User's data",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Post"
              }
            }
          }
        },
        
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/posts/{post_id}/destroy": {
      "delete": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "Delete post",
        "description": "Delete post",
        "operationId": "Delete post",
        "parameters": [
          {
            "name": "post_id",
            "required": true,
            "type": "integer",
            "in": "path"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/favorite-posts": {
      "get": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "List of favorite posts",
        "description": "List of favorite posts",
        "operationId": "List of favorite posts",

        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/favorite-posts/{post_id}": {
      "get": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "Add to favorite posts",
        "description": "Add to favorite posts",
        "operationId": "Add to favorite posts",
        "parameters": [
          {
            "name": "post_id",
            "required": true,
            "type": "integer",
            "in": "path"
          }
        ],

        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    },
    "/favorite-posts/{post_id}/destroy": {
      "delete": {
        "tags": [
          "Posts"
        ],
        "security": [{
          "bearerAuth": []
        }],
        "deprecated": false,
        "summary": "Delete post from favorite list",
        "description": "Delete post from favorite list",
        "operationId": "Delete post from favorite list",
        "parameters": [
          {
            "name": "post_id",
            "required": true,
            "type": "integer",
            "in": "path"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Bad request"
          }
        }
      }
    }
  }
}
